#!/bin/bash
#
# This script is almost entirely built on the build script from redmine_backlogs
# Please see: https://github.com/backlogs/redmine_backlogs
#

#if [[ -e "$HOME/.xapian.rc" ]]; then
#  source "$HOME/.xapian.rc"
#fi

export WORKSPACE=`pwd`/workspace
export PATH_TO_XAPIAN=`pwd`
export PATH_TO_REDMINE=$WORKSPACE/redmine
export REDMINE_GIT_REPO=git://github.com/redmine/redmine.git
export REDMINE_GIT_TAG=3.4-stable

clone()
{
  set -e # exit if clone fails

  rm -rf $WORKSPACE
  mkdir -p $WORKSPACE
  rm -rf $PATH_TO_REDMINE
  git clone -b $REDMINE_GIT_TAG --depth=100 --quiet $REDMINE_GIT_REPO $PATH_TO_REDMINE
}

test()
{
  # Exit if tests fail
  set -e

  cd $PATH_TO_REDMINE

  # Run tests within application
  bundle exec rake redmine:plugins:test:units NAME=redmine_xapian RAILS_ENV=test
  bundle exec rake redmine:plugins:test:functionals NAME=redmine_xapian RAILS_ENV=test
}

uninstall()
{
   # Exit if the uninstallation fails
  set -e

  cd $PATH_TO_REDMINE

  # clean up database
  # TODO: Plugin redmine_xapian not found
  #bundle exec rake redmine:plugins:migrate NAME=redmine_xapian VERSION=0 RAILS_ENV=test
}

install()
{
  # Exit if the installation fails
  set -e

  ruby --version
  bundle --version
  echo Gem `gem --version`

  # cd to redmine folder
  cd $PATH_TO_REDMINE
  echo current directory is `pwd`

  # Create a link to the Xapian plugin
  ln -sf $PATH_TO_XAPIAN plugins/redmine_xapian

  # Install gems
  mkdir -p vendor/bundle
}

migrate()
{
  # Exit if a migration fails
  set -e

  cd $PATH_TO_REDMINE

  # Copy database.yml
  cp -f "${PATH_TO_XAPIAN}/test/$1.yml" config/database.yml

  bundle install --without rmagick development --path vendor/bundle

  # Run redmine database migrations
  bundle exec rake db:migrate RAILS_ENV=test --trace

  # Load redmine database default data
  bundle exec rake redmine:load_default_data REDMINE_LANG=en RAILS_ENV=test

  # Generate session store/secret token
  bundle exec rake generate_secret_token

  # Run Xapian database migrations
  bundle exec rake redmine:plugins:migrate RAILS_ENV=test
}

while getopts :ictump opt
do case "$opt" in
  c) clone; exit 0;;
  i) install; exit 0;;
  m) migrate mysql; exit 0;;
  p) migrate postgres; exit 0;;
  t) test; exit 0;;
  u) uninstall; exit 0;;
  [?]) echo "i: install; r: clone redmine; t: run tests; u: uninstall; m: migrate to mysql; p: migrate to postgres";;
  esac
done